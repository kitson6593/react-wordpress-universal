# Steps to launch:

1. install wordpress to "wp" directory at root of project and configure db
git clone https://github.com/WordPress/WordPress wp

2. install node dependencies
npm i

3. install babel-node client
npm install -g babel-cli

4. run node
babel-node src/server.jsx

5. update src/config.jsx - set URL to wpapi (wp-json)

5. launch in browser
http://localhost:3000/

====================================================================

# Trouble shooting:

# bootstrap (install dependencies for custom builds via grunt)
ref: https://v4-alpha.getbootstrap.com/getting-started/build-tools/
cd node_modules/bootstrap
npm install
sudo gem install bundler
bundle init
sudo gem update --system && gem install scss-lint
bundle install
grunt