import React from 'react';
import {Provider} from 'react-redux';
import {createStore} from 'redux';
import {connect} from 'react-redux';
import config from './config'

let initialState = {
    posts: [],
    api: config.wpapi,
    pollInterval: 20
};

let reducer = function (state, action) {

    if (state === undefined || state.length === 0) {
        return initialState;
    }

    let newState;

    switch (action.type) {
        case 'UPDATE_POSTS':
            newState = Object.assign({}, state, { posts: action.posts } );
            break;
        default:
            newState = state;
    }

    return newState;

};

module.exports = reducer;