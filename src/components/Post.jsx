import React from 'react'
import RawHtml from "react-raw-html"

export default function Post(props) {
    return (
    	<div className="col-md-4">
			<h3>{props.title}</h3>
			<RawHtml.div>{props.html}</RawHtml.div>
		</div>
    );
}