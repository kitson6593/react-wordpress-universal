import React from 'react'
import {Link} from 'react-router';

export default class Nav extends React.Component {
    render() {
        return (
            <div className="row">
                <div className="col-lg-3 col-md-12">
                    <h3 className="text-muted">Universal<br/>React / Redux</h3>
                </div>
                <div className="col-lg-6 col-md-12">
                    <nav className="navbar navbar-light">
                        <ul className="nav navbar-nav">
                            <li className="nav-item active">
                                <Link to="/">Home <span className="sr-only">(current)</span></Link>
                            </li>
                            <li className="nav-item">
                                <Link to="/posts">Posts</Link>
                            </li>
                            <li className="nav-item">
                                <Link to="/about">About</Link>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div className="col-lg-3 col-md-12" id="quick-links">
                    <button type="button" className="btn btn-secondary btn-md btn-block">Join the WADG</button>
                </div>
            </div>
        )
    }
}