import React from 'react'
import WPAPI from 'wpapi'
import { Link } from 'react-router'
import { connect } from 'react-redux'
import {bindActionCreators} from 'redux';
import Post from './Post';
import config from '../config'
import * as Actions from '../actions'

class Posts extends React.Component {

	constructor(props, context) {
		super(props, context);
	}

	componentDidMount() {

		var wp = new WPAPI({ endpoint: config.wpapi }), _this = this;
		wp.posts().then(function( data ) {

			_this.props.actions.updatePosts(data);

		}).catch(function( err ) {
			console.log("issue");
		});

	}

	render() {
		console.log(this);
		return (
			<div className="row">
				{this.props.posts.map(function(p) {
					return <Post key={p.id} title={p.title.rendered} html={p.content.rendered} link={p.link} />
				})}
			</div>
		)
	}
}

function mapStateToProps(state) {
	return { posts: state.posts };
}

function mapDispatchToProps(dispatch) {
	return {
		actions: bindActionCreators(Actions, dispatch)
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Posts)
