import React from 'react'
import RawHtml from "react-raw-html"

export default function Page(props) {
    return (
    	<div>
			<h3>{props.title}</h3>
			<RawHtml.div>{props.html}</RawHtml.div>
		</div>
    );
}