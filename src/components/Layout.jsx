import React from 'react';
import { Link } from 'react-router';
import Header from './Header'
import Footer from './Footer'

export default class Layout extends React.Component {
  render() {
    return (
      <div className="app-container">
        <div id="header" className="header clearfix"><Header /></div>
        <div className="app-content">{this.props.children}</div>
        <div id="footer"><Footer /></div>
      </div>
    );
  }
}