import React from 'react'

export default function About() {
    return (
    	<div>
    		<h2>About Us</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt pharetra erat, id porttitor sapien faucibus id. Ut enim velit, gravida in porttitor eu, lobortis quis nunc. Nulla vel massa mauris. Vestibulum vestibulum, lorem ut ultrices faucibus, turpis libero imperdiet tellus, at finibus dui quam sit amet ante. Fusce est magna, dictum ac pharetra in, egestas vitae tortor. Suspendisse mollis augue nec arcu ullamcorper, quis semper nulla dignissim. Sed et neque auctor, elementum sem ut, eleifend arcu. Quisque dignissim dignissim justo. Cras id varius ligula. Fusce pulvinar lorem sed lobortis elementum. Nulla pretium consectetur tincidunt.</p>
			<p>Aenean egestas eros nec ex vehicula, elementum luctus magna volutpat. Duis fermentum leo in turpis vulputate interdum. Nunc eu condimentum est, non consectetur diam. Integer interdum urna augue, ut sollicitudin dui rutrum eu. Sed tincidunt pharetra lectus ut iaculis. Ut nec sagittis dui. In diam ante, porta ac molestie non, luctus id risus. Integer id lorem imperdiet diam elementum scelerisque id at dolor. Maecenas sed fermentum turpis. Nulla facilisi. Donec velit nunc, finibus ac pharetra quis, blandit consectetur massa. Sed augue nunc, vulputate sagittis posuere eget, faucibus vel sem. Morbi ut ante eu lacus interdum viverra at sit amet ante. Quisque porta commodo fermentum. Suspendisse vitae elit sed dolor blandit tempor.</p>
    	</div>
    );
}
