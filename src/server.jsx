import path from 'path';
import {Server} from 'http';
import Express from 'express';
import React from 'react';
import WPAPI from 'wpapi';
import {renderToString} from 'react-dom/server';
import {match, RouterContext} from 'react-router';
import routes from './routes';
import NotFoundPage from './components/NotFound';
import reducer from './reducer'
import {createStore} from 'redux'
import {Provider} from 'react-redux'
import config from './config'

// initialize the server and configure support for ejs templates
const app = new Express();
const server = new Server(app);
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));

// define the folder that will be used for static assets
app.use(Express.static(path.join(__dirname, 'dist')));

// universal routing and rendering
app.get('*', (req, res) => {
    match(
        {routes, location: req.url},
        (err, redirectLocation, renderProps) => {

            // in case of error display the error message
            if (err) {
                return res.status(500).send(err.message);
            }

            // in case of redirect propagate the redirect to the browser
            if (redirectLocation) {
                return res.redirect(302, redirectLocation.pathname + redirectLocation.search);
            }

            // generate the React markup for the current route
            let markup, preloadedState = [];

            // ***************************************************

            // // create redux store from json data from wp
            // const store = createStore(reducer, []);

            // // generate react markup
            // if (renderProps) {
            //     markup = renderToString(
            //         <Provider store={store}>
            //             <RouterContext {...renderProps}/>
            //         </Provider>
            //     );
            // } else {
            //     // otherwise we can render a 404 page
            //     markup = renderToString(<NotFoundPage/>);
            //     res.status(404);
            // }

            // // grab initial state
            // preloadedState = JSON.stringify([]);

            // // render the index template with react markup and initial state
            // return res.render('index', { markup, preloadedState } );

            // ***************************************************

            // wp api
            let wp = new WPAPI( { endpoint: config.wpapi } );
            
            wp.posts().then(function (data) {
            
                const state = {};
                state.posts = data;
            
                // create redux store from json data from wp
                const store = createStore(reducer, state);
            
                // generate react markup
                if (renderProps) {
                    markup = renderToString(
                        <Provider store={store}>
                            <RouterContext {...renderProps}/>
                        </Provider>
                    );
                } else {
                    // otherwise we can render a 404 page
                    markup = renderToString(<NotFoundPage/>);
                    res.status(404);
                }
            
                // grab initial state
                preloadedState = JSON.stringify(store.getState());
            
                // render the index template with react markup and initial state
                return res.render('index', { markup, preloadedState } );
            
            }).catch(function (err) {
                console.log(err);
            });

            // ***************************************************

        }
    );
});

// start the server
const port = process.env.PORT || 3000;
const env = process.env.NODE_ENV || 'production';
server.listen(port, err => {
    if (err) {
        return console.error(err);
    }
    console.info(`Server running on http://localhost:${port} [${env}]`);
});