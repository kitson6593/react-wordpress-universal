export const GET_POSTS = 'GET_POSTS';
export const UPDATE_POSTS = 'UPDATE_POSTS';


export const getPosts = () => ({
    type: GET_POSTS
});

export const updatePosts = (posts) => ({
    type: UPDATE_POSTS,
    posts
});