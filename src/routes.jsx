import React from 'react'
import {Route, IndexRoute} from 'react-router'
import Layout from './components/Layout';
import Home from './components/Home';
import About from './components/About';
import Posts from './components/Posts';
import NotFound from './components/NotFound';

const routes = (
    <Route path="/" component={Layout}>
        <IndexRoute component={Home}/>
        <Route path="home" component={Home}/>
        <Route path="posts" component={Posts}/>
        <Route path="about" component={About}/>
        <Route path="*" component={Home}/>
    </Route>
);

export default routes;