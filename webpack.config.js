var path = require("path"), webpack = require("webpack"), ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = {
    entry: {
        main: path.join(__dirname, 'src', 'app-client.jsx'),
    },
    output: {
        filename: "./src/dist/js/[name].bundle.js",
        chunkFilename: "./dist/dist/[id].bundle.js",
        publicPath: '../../'
    },
    resolve: {
        extensions: ['', '.webpack.js', '.web.js', '.js', '.jsx', '.json']
    },
    module: {
        loaders: [
            { test: /\.js$/, loader: 'babel-loader' },
            { test: /\.jsx$/, loader: 'babel-loader' },
            { test: /\.less$/, exclude: /node_modules/, loader: ExtractTextPlugin.extract("style-loader", "css-loader!less-loader") },
            { test: /\.scss$/, exclude: /node_modules/, loader: ExtractTextPlugin.extract("style-loader", "css-loader!sass-loader") },
            // { test: /\.png$/, loader: "url-loader?limit=100000&name=src/dist/images/[hash].[ext]" },
            // { test: /\.svg$/, loader: "url-loader?limit=100000&name=src/dist/images/[hash].[ext]" },
            // { test: /\.jpg$/, loader: "url-loader?limit=100000&name=src/dist/images/[hash].[ext]" },
            { test: /\.json$/, loader: "json-loader" }
        ]
    },
    devtool: 'inline-source-map',
    eslint: {
        configFile: './.eslintrc'
    },
    plugins: [
        new ExtractTextPlugin('./src/dist/css/[name].css', {
            allChunks: true
        }),
        // new webpack.optimize.UglifyJsPlugin({
        //     compressor: { warnings: false }
        // }),
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV || 'development')
        })
    ]
};